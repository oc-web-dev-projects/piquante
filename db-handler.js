const mongoose = require('mongoose')
/**
* Connect to the database.
*/
module.exports.connect = async (uri) => {
  await mongoose.connect(uri,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })
    .then(() => console.log('Connexion à MongoDB réussie !'))
    .catch((err) => console.log('Connexion à MongoDB échouée !', err))
}

/**
* Drop database, close the connection and stop mongod.
*/
module.exports.closeDatabase = async () => {
  await mongoose.connection.close()
}

/**
* Remove all the data for all db collections.
*/
module.exports.clearDatabase = async () => {
  const collections = mongoose.connection.collections

  for (const key in collections) {
    const collection = collections[key]
    try {
      await collection.deleteMany({})
    } catch (err) {
      console.error(err)
    }
  }
}
