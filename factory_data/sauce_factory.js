const faker = require('faker')

class FactorySauce {
  constructor (name, heat) {
    this.userId = `${faker.random.number()}`
    this.name = name || faker.name.findName()
    this.manufacturer = faker.company.companyName()
    this.description = faker.lorem.sentence()
    this.mainPepper = faker.lorem.word()
    this.imageUrl = faker.image.imageUrl()
    this.heat = heat || Math.floor(Math.random() * (10 - 1)) + 1
  }
}

const getSauceSample = () => {
  const data = []
  for (let i = 0; i < 10; i++) {
    const sauce = new FactorySauce()
    data.push(sauce)
  }
  return data
}

module.exports = { FactorySauce, getSauceSample }
