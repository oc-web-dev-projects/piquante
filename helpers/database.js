require('dotenv').config()

const getConnectionUri = (isTest = false) => {
  if (isTest) {
    return `mongodb://${process.env.DATABASE_TEST_USERNAME}:${process.env.DATABASE_TEST_PASSWORD}@${process.env.DATABASE_TEST_HOST}:${process.env.DATABASE_TEST_PORT}/${process.env.DATABASE_TEST_NAME}`
  } else {
    return `${process.env.DATABASE_URI_PREFIX}://${process.env.DATABASE_USERNAME}:${process.env.DATABASE_PASSWORD}@${process.env.DATABASE_HOST}/${process.env.DATABASE_NAME}${process.env.DATABASE_OPTIONS}`
  }
}

module.exports = { getConnectionUri }
