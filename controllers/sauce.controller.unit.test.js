const sauceController = require('./sauce.controller')
const SauceModel = require('../models/sauce.model')
const httpMocks = require('node-mocks-http')
const fs = require('fs')
const { FactorySauce } = require('../factory_data/sauce_factory')

jest.mock('../models/sauce.model')

let req, res, next, sauce

beforeEach(() => {
  req = httpMocks.createRequest()
  res = httpMocks.createResponse()
  next = jest.fn()
  sauce = new FactorySauce()
})

describe('sauceController.createSauce', () => {
  test('should have a createSauce function', () => {
    expect(sauceController.createSauce).toBeInstanceOf(Function)
  })
  test('should return a 201 response code if sauce info is provided', async () => {
    req.body.sauce = JSON.stringify(sauce)
    req.protocol = 'http'
    req.file = { filename: 'myImage' }
    try {
      await sauceController.createSauce(req, res, next)
      expect(res.statusCode).toBe(201)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return a 400 response code if sauce property is missing', async () => {
    sauce.heat = undefined
    req.body.sauce = JSON.stringify(sauce)
    req.protocol = 'http'
    req.file = { filename: 'image' }
    const errorMessage = { message: 'property missing' }
    const SauceModelMock = jest.spyOn(SauceModel.prototype, 'save')
    SauceModelMock.mockRejectedValue(errorMessage)
    try {
      await sauceController.createSauce(req, res, next)
      expect(res.statusCode).toBe(400)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    try {
      await sauceController.createSauce(req, res, next)
      expect(res.statusCode).toBe(500)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})
describe('sauceController.getAllSauces', () => {
  const allSauces = [new FactorySauce(), new FactorySauce()]
  test('should have a getAllSauces function', () => {
    expect(sauceController.getAllSauces).toBeInstanceOf(Function)
  })
  test('should call SauceModel.find', async () => {
    try {
      await sauceController.getAllSauces(req, res, next)
      expect(SauceModel.find).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and all sauces', async () => {
    SauceModel.find.mockReturnValue(allSauces)
    try {
      await sauceController.getAllSauces(req, res, next)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData()).toEqual(allSauces)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    const errorMessage = 'Not found'
    SauceModel.find.mockRejectedValue(errorMessage)
    try {
      await sauceController.getAllSauces(req, res, next)
      expect(res.statusCode).toBe(400)
      expect(res._getJSONData().error).toBe(errorMessage)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('sauceController.getOneSauce', () => {
  test('should have a getOneSauce function', () => {
    expect(sauceController.getOneSauce).toBeInstanceOf(Function)
  })
  test('should call SauceModel.findOne', async () => {
    try {
      await sauceController.getOneSauce(req, res, next)
      expect(SauceModel.findOne).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and the sauce', async () => {
    req.params.id = '25'
    SauceModel.findOne.mockReturnValue(sauce)
    try {
      await sauceController.getOneSauce(req, res, next)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData()).toEqual(sauce)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.params.id = '25'
    const errorMessage = 'Not found'
    SauceModel.findOne.mockRejectedValue(errorMessage)
    try {
      await sauceController.getOneSauce(req, res, next)
      expect(res.statusCode).toBe(404)
      expect(res._getJSONData().error).toBe(errorMessage)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('sauceController.deleteSauce', () => {
  test('should have a deleteOne function', () => {
    expect(sauceController.deleteSauce).toBeInstanceOf(Function)
  })
  test('should call SauceModel.findOne', async () => {
    jest.spyOn(fs, 'unlink')
    try {
      await sauceController.deleteSauce(req, res, next)
      expect(SauceModel.findOne).toBeCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should call fs.unlink with filename and anonymous function', async () => {
    req.params.id = '25'
    SauceModel.findOne.mockReturnValue({ imageUrl: '/images/mysauce.png' })
    const fsMock = jest.spyOn(fs, 'unlink')
    sauceController.deleteCallback = jest.fn()
    try {
      await sauceController.deleteSauce(req, res, next)
      expect(fsMock).toBeCalledWith('images/mysauce.png', expect.any(Function))
      fsMock.mockRestore()
    } catch (error) {
      console.log(error)
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    const errorMessage = 'Bad request'
    req.params.id = '23'
    SauceModel.findOne.mockRejectedValue(errorMessage)
    try {
      await sauceController.deleteSauce(req, res, next)
      expect(res.statusCode).toBe(500)
      expect(res._getJSONData().error).toBe(errorMessage)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
})

describe('sauceController.modifySauce', () => {
  beforeEach(() => { SauceModel.findOne.mockReturnValue({ imageUrl: '/images/mysauce.png' }) })
  test('should have a modifySauce function', () => {
    expect(sauceController.modifySauce).toBeInstanceOf(Function)
  })
  test('should call SauceModel.updateOne', async () => {
    req.body = { sauce: sauce }
    req.params = { id: 51 }
    try {
      await sauceController.modifySauce(req, res, next)
      expect(SauceModel.updateOne).toBeCalled()
    } catch (error) {
      console.log('first failed test', error)
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and message without file', async () => {
    req.body = { sauce: sauce }
    req.params = { id: 51 }
    try {
      await sauceController.modifySauce(req, res, next)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Sauce modifiée !')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should return response with status 200 and message with file', async () => {
    req.body.sauce = JSON.stringify(sauce)
    req.protocol = 'http'
    req.file = { filename: 'myImage' }
    req.params = { id: 51 }
    try {
      await sauceController.modifySauce(req, res, next)
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Sauce modifiée !')
    } catch (error) {
      console.log(error)
      expect(true).toBe(false)
    }
  })
  test('should call findOne and remove old file when updated with new file', async () => {
    req.body.sauce = JSON.stringify(sauce)
    req.protocol = 'http'
    req.file = { filename: 'myImage' }
    req.params = { id: 51 }
    const fsMock = jest.spyOn(fs, 'unlink')
    sauceController.deleteCallback = jest.fn()
    try {
      await sauceController.modifySauce(req, res, next)
      expect(SauceModel.findOne).toBeCalled()
      expect(fsMock).toBeCalledWith('images/mysauce.png', expect.any(Function))
      fsMock.mockRestore()
    } catch (error) {
      console.log(error)
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.body = { sauce: sauce }
    req.params = { id: 51 }
    SauceModel.updateOne.mockRejectedValue()
    try {
      await sauceController.modifySauce(req, res, next)
      expect(res.statusCode).toBe(400)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
})

describe('sauceController.changeLikeSauce', () => {
  beforeEach(() => {
    Object.defineProperty(sauce, 'save', {
      writable: true,
      value: jest.fn().mockImplementation(query => ({
        matches: false,
        media: query,
        onchange: null,
        addListener: jest.fn(), // deprecated
        removeListener: jest.fn(), // deprecated
        addEventListener: jest.fn(),
        removeEventListener: jest.fn(),
        dispatchEvent: jest.fn()
      }))
    })
    sauce.usersLiked = ['1', '5']
    sauce.usersDisliked = ['2', '3']
  })
  test('should have a changeLikeSauce function', () => {
    expect(sauceController.changeLikeSauce).toBeInstanceOf(Function)
  })
  test('should call findOne', async () => {
    req.body.userId = '3'
    req.body.like = 1
    SauceModel.findOne.mockReturnValue(sauce)
    jest.spyOn(SauceModel.prototype, 'save')
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(SauceModel.findOne).toBeCalled()
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=1 should add like if user has not already liked', async () => {
    req.body.userId = '3'
    req.body.like = 1
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.likes = 10
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.likes).toEqual(11)
      expect(sauce.usersLiked).toEqual(['1', '5', '3'])
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Nombre de likes mis à jour !')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=1 remove dislike', async () => {
    req.body.userId = '3'
    req.body.like = 1
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.likes = 10
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.likes).toEqual(11)
      expect(sauce.usersLiked).toEqual(['1', '5', '3'])
      expect(sauce.usersDisliked).toEqual(['2'])
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=1 should not add like and return a 200 status code', async () => {
    req.body.userId = '5'
    req.body.like = 1
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.likes = 10
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.likes).toEqual(10)
      expect(sauce.usersLiked).toEqual(['1', '5'])
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=-1 should add dislike and return a 200 status code', async () => {
    req.body.userId = '6'
    req.body.like = -1
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.dislikes = 2
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.dislikes).toEqual(3)
      expect(sauce.usersDisliked).toEqual(['2', '3', '6'])
      expect(res.statusCode).toEqual(200)
      expect(res._getJSONData().message).toBe('Nombre de likes mis à jour !')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=-1 should remove like', async () => {
    req.body.userId = '1'
    req.body.like = -1
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.dislikes = 2
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.dislikes).toEqual(3)
      expect(sauce.usersDisliked).toEqual(['2', '3', '1'])
      expect(sauce.usersLiked).toEqual(['5'])
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=-1 should not add dislike if user has already disliked', async () => {
    req.body.userId = '3'
    req.body.like = -1
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.dislikes = 2
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.dislikes).toEqual(2)
      expect(sauce.usersDisliked).toEqual(['2', '3'])
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=0 should remove like', async () => {
    req.body.userId = '1'
    req.body.like = 0
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.likes = 2
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.likes).toEqual(1)
      expect(sauce.usersLiked).toEqual(['5'])
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Nombre de likes mis à jour !')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('like=0 should remove dislike', async () => {
    req.body.userId = '2'
    req.body.like = 0
    sauce.dislikes = 2
    SauceModel.findOne.mockReturnValue(sauce)
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(sauce.dislikes).toEqual(1)
      expect(sauce.usersDisliked).toEqual(['3'])
      expect(res.statusCode).toBe(200)
      expect(res._getJSONData().message).toBe('Nombre de likes mis à jour !')
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors on save', async () => {
    Object.defineProperty(sauce, 'save', {
      writable: true,
      value: jest.fn().mockRejectedValue()
    })
    req.body.userId = '2'
    req.body.like = 0
    SauceModel.findOne.mockReturnValue(sauce)
    sauce.dislikes = 2
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(res.statusCode).toBe(400)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors on find', async () => {
    req.body.userId = '1'
    req.body.like = -1
    SauceModel.findOne.mockRejectedValue()
    try {
      await sauceController.changeLikeSauce(req, res, next)
      expect(res.statusCode).toBe(500)
    } catch (error) {
      expect(true).toBe(false)
    }
  })
})
