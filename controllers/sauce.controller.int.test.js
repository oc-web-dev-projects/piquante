const app = require('../app')
const request = require('supertest')
const auth = require('../middleware/auth')
const SauceModel = require('../models/sauce.model')
const dbHandler = require('../db-handler')
const { getSauceSample } = require('../factory_data/sauce_factory')
const { getConnectionUri } = require('../helpers/database')

jest.mock('../middleware/auth')
auth.mockImplementation((req, res, next) => { return next() })

const endpointUrl = '/api/sauces/'

beforeAll(async () => {
  await dbHandler.connect(getConnectionUri(true))
  const data = getSauceSample()
  await SauceModel.insertMany(data)
})

afterAll(async () => {
  await dbHandler.clearDatabase()
  await dbHandler.closeDatabase()
})

describe(endpointUrl, () => {
  let firstSauce
  test('GET' + endpointUrl, async () => {
    const response = await request(app)
      .get(endpointUrl)
    expect(response.statusCode).toBe(200)
    firstSauce = response.body[0]
  })
  test('GET' + endpointUrl + ':id', async () => {
    const id = firstSauce._id
    const response = await request(app)
      .get(endpointUrl + id)
    expect(response.statusCode).toBe(200)
    expect(response.body.name).toBe(firstSauce.name)
  })
  test("GET sauce by id doesn't exist " + endpointUrl + ':id', async () => {
    const fakeId = '5e42438873886f3f0ef45e7888'
    const response = await request(app)
      .get(endpointUrl + fakeId)
    expect(response.statusCode).toBe(404)
  })
  test('PUT' + endpointUrl + ':id', async () => {
    const expectedMsg = 'Sauce modifiée !'
    const id = firstSauce._id
    const testData = {
      userId: 1,
      name: 'Red Tabasco',
      manufacturer: 'Hoppe, Rosenbaum and Crist',
      description: 'Quasi molestiae ut molestiae sed.',
      mainPepper: 'error',
      imageUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/sethlouey/128.jpg',
      heat: 1
    }
    const response = await request(app)
      .put(endpointUrl + id)
      .send(testData)
    expect(response.statusCode).toBe(200)
    expect(response.body.message).toBe(expectedMsg)
  })
  test('POST like' + endpointUrl + ':id' + '/like', async () => {
    const id = firstSauce._id
    const testData = { userId: '10', like: 1 }
    const response = await request(app)
      .post(endpointUrl + id + '/like')
      .send(testData)
    expect(response.statusCode).toBe(200)
    expect(response.body.message).toBe('Nombre de likes mis à jour !')
  })
  test('POST dilike' + endpointUrl + ':id' + '/like', async () => {
    const id = firstSauce._id
    const testData = { userId: '10', like: -1 }
    const response = await request(app)
      .post(endpointUrl + id + '/like')
      .send(testData)
    expect(response.statusCode).toBe(200)
    expect(response.body.message).toBe('Nombre de likes mis à jour !')
  })
  test('DELETE' + endpointUrl + ':id', async () => {
    const id = firstSauce._id
    const response = await request(app)
      .delete(endpointUrl + id)
    expect(response.statusCode).toBe(200)
    expect(response.body.message).toBe('Sauce supprimée !')
  })
  test('DELETE HTML 500', async () => {
    const nonExistingId = '5e42438873886f3f0ef45e7888'
    const response = await request(app)
      .delete(endpointUrl + nonExistingId)
    expect(response.statusCode).toBe(500)
  })
  test('POST' + endpointUrl, async () => {
    const newSauceObject = {
      userId: '51',
      name: 'Green Jalapeno sauce',
      manufacturer: 'McIlhenny Company',
      description: 'Not too hot, not too mild, TABASCO® brand Green Jalapeño Pepper Sauce is a must for Mexican food.',
      mainPepper: 'jalapeño pepper',
      heat: 2
    }
    const newSauce = JSON.stringify(newSauceObject)
    const response = await request(app)
      .post(endpointUrl)
      .attach('image', 'images/sauce.png', { filename: 'mysauce' })
      .field('sauce', newSauce)
    expect(response.statusCode).toBe(201)
    expect(response.body.message).toBe('Sauce créée')
  })
  test('should return error 500 on malformed data with POST', async () => {
    const newSauceObject = {
      userId: '51',
      name: 'My sauce',
      manufacturer: 'McIlhenny Company',
      description: 'Not too hot, not too mild, TABASCO® brand Green Jalapeño Pepper Sauce is a must for Mexican food.',
      mainPepper: 'jalapeño pepper',
      heat: 2
    }
    const newSauce = JSON.stringify(newSauceObject)
    const response = await request(app)
      .post(endpointUrl)
      .send(newSauce)
    expect(response.statusCode).toBe(500)
  })
})
