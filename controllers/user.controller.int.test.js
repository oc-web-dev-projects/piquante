const app = require('../app')
const request = require('supertest')
const dbHandler = require('../db-handler')
const { getConnectionUri } = require('../helpers/database')

const endpointUrl = '/api/auth/'
const email = 'mytest@integration.com'
const password = 'integratiotestpassword'

beforeAll(async () => {
  await dbHandler.connect(getConnectionUri(true))
})

afterAll(async () => {
  await dbHandler.clearDatabase()
  await dbHandler.closeDatabase()
})

describe(endpointUrl, () => {
  test('POST signup', async () => {
    const response = await request(app)
      .post(endpointUrl + 'signup')
      .send({ email: email, password: password })
    expect(response.statusCode).toBe(201)
    expect(response.body.message).toBe('Utilisateur créé')
  })
  test('POST signup duplicate email HTML 400', async () => {
    const response = await request(app)
      .post(endpointUrl + 'signup')
      .send({ email: email, password: 'mytestpassword' })
    expect(response.statusCode).toBe(400)
  })
  test('POST signup  required field HTML 500', async () => {
    const response = await request(app)
      .post(endpointUrl + 'signup')
      .send({ email: 'test@test.com' })
    expect(response.statusCode).toBe(500)
  })
  test('POST login', async () => {
    const response = await request(app)
      .post(endpointUrl + 'login')
      .send({ email: email, password: password })
    expect(response.statusCode).toBe(200)
    expect(response.body.userId).toBeDefined()
    expect(response.body.token).toBeDefined()
  })
  test('POST login 401 invalid user', async () => {
    const response = await request(app)
      .post(endpointUrl + 'login')
      .send({ email: 'wrong@email.com', password: password })
    expect(response.statusCode).toBe(401)
    expect(response.body.error).toEqual('Utilisateur non trouvé')
  })
  test('POST login 401 invalid password', async () => {
    const response = await request(app)
      .post(endpointUrl + 'login')
      .send({ email: email, password: 'wrongpassword' })
    expect(response.statusCode).toBe(401)
    expect(response.body.error).toEqual('Mot de passe incorrect')
  })
})
