const userController = require('./user.controller')
const userModel = require('../models/user.model')
const httpMocks = require('node-mocks-http')
const bcrypt = require('bcrypt')

jest.mock('../models/user.model')

let req, res, next, userMock, expectedMsg

beforeEach(() => {
  req = httpMocks.createRequest()
  res = httpMocks.createResponse()
  next = jest.fn()
  userMock = { _id: 145, email: 'test@example.com', password: '%testpassword01' }
})

describe('userController.signup', () => {
  test('should have a signup function', () => {
    expect(userController.signup).toBeInstanceOf(Function)
  })
  test('should return a 201 response code if email and password are provided', async () => {
    userMock = { email: 'test@example.com', password: '%testpassword01' }
    req.body = userMock
    try {
      await userController.signup(req, res, next)
      expect(res.statusCode).toBe(201)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should return error responde code if user information is missing', async () => {
    req.body = {}
    try {
      await userController.signup(req, res, next)
      expect(res.statusCode).toBe(500)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors', async () => {
    req.body = userMock
    const errorMessage = { message: 'property missing' }
    const userModelMock = jest.spyOn(userModel.prototype, 'save')
    userModelMock.mockRejectedValue(errorMessage)
    try {
      await userController.signup(req, res, next)
      expect(res.statusCode).toBe(400)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})

describe('userController.login', () => {
  test('should be have a login function', () => {
    expect(userController.login).toBeInstanceOf(Function)
  })
  test('should return a 401 response status code if no user is found with the given email', async () => {
    req.body = userMock
    userModel.findOne.mockReturnValue(null)
    expectedMsg = 'Utilisateur non trouvé'
    try {
      await userController.login(req, res, next)
      const data = res._getJSONData()
      expect(res.statusCode).toBe(401)
      expect(data.error).toEqual(expectedMsg)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should compare DB and request password and return 401 status code if they do not match', async () => {
    const returnedUser = { email: 'test@example.com', password: 'testpassword' }
    userModel.findOne.mockReturnValue(returnedUser)
    jest.spyOn(bcrypt, 'compare').mockReturnValue(false)
    req.body = userMock
    expectedMsg = 'Mot de passe incorrect'
    try {
      await userController.login(req, res, next)
      const data = res._getJSONData()
      expect(res.statusCode).toBe(401)
      expect(data.error).toEqual(expectedMsg)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should compare DB and request password and return 200 status code if they match', async () => {
    userModel.findOne.mockReturnValue(userMock)
    jest.spyOn(bcrypt, 'compare').mockReturnValue(true)
    req.body = userMock
    try {
      await userController.login(req, res, next)
      const data = res._getJSONData()
      expect(res.statusCode).toBe(200)
      expect(data.userId).toEqual(userMock._id)
    } catch (err) {
      console.log(err)
      expect(true).toBe(false)
    }
  })
  test('should handle errors if bcrypt rejects', async () => {
    userModel.findOne.mockReturnValue(userMock)
    const fakeErrorMsg = 'fake error'
    jest.spyOn(bcrypt, 'compare').mockRejectedValue(fakeErrorMsg)
    req.body = userMock
    try {
      await userController.login(req, res, next)
      const data = res._getJSONData()
      expect(res.statusCode).toBe(500)
      expect(data.error).toBe(fakeErrorMsg)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should handle errors if Model.findOne rejects', async () => {
    const fakeErrorMsg = 'fake error'
    userModel.findOne.mockRejectedValue(fakeErrorMsg)
    req.body = userMock
    try {
      await userController.login(req, res, next)
      const data = res._getJSONData()
      expect(res.statusCode).toBe(500)
      expect(data.error).toBe(fakeErrorMsg)
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})
