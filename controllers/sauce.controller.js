const SauceModel = require('../models/sauce.model')
const fs = require('fs')

exports.createSauce = async (req, res, next) => {
  try {
    const sauceObject = JSON.parse(req.body.sauce)
    const sauce = new SauceModel({
      ...sauceObject,
      imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
    })
    try {
      await sauce.save()
      res.status(201).json({ message: 'Sauce créée' })
    } catch (error) {
      res.status(400).json({ error })
    }
  } catch (error) {
    res.status(500).json({ error })
  }
}

exports.getAllSauces = async (req, res, next) => {
  try {
    const sauces = await SauceModel.find()
    res.status(200).json(sauces)
  } catch (error) {
    res.status(400).json({ error })
  }
}

exports.getOneSauce = async (req, res, next) => {
  try {
    const sauce = await SauceModel.findOne({ _id: req.params.id })
    res.status(200).json(sauce)
  } catch (error) {
    res.status(404).json({ error })
  }
}

exports.deleteSauce = async (req, res, next) => {
  try {
    const sauce = await SauceModel.findOne({ _id: req.params.id })
    const filename = sauce.imageUrl.split('/images/')[1]
    fs.unlink(`images/${filename}`, async () => {
      try {
        await SauceModel.deleteOne({ _id: req.params.id })
        res.status(200).json({ message: 'Sauce supprimée !' })
      } catch (error) {
        res.status(400).json({ error })
      }
    })
  } catch (error) {
    res.status(500).json({ error })
  }
}

exports.modifySauce = async (req, res, next) => {
  try {
    const sauce = await SauceModel.findOne({ _id: req.params.id })
    const filename = sauce.imageUrl.split('/images/')[1]
    if (req.file) { fs.unlink(`images/${filename}`, (err) => { if (err) res.status(500).json({ err }) }) }
    const sauceObject = req.file
      ? {
        ...JSON.parse(req.body.sauce),
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
      } : { ...req.body }
    await SauceModel.updateOne({ _id: req.params.id }, { ...sauceObject, _id: req.params.id })
    res.status(200).json({ message: 'Sauce modifiée !' })
  } catch (error) {
    res.status(400).json({ error })
  }
}

exports.changeLikeSauce = async (req, res, next) => {
  try {
    const userId = req.body.userId
    const like = req.body.like
    const sauce = await SauceModel.findOne({ _id: req.params.id })
    const usersLiked = sauce.usersLiked
    const usersDisliked = sauce.usersDisliked
    const doesUserLike = usersLiked.includes(userId)
    const doesUserDislike = usersDisliked.includes(userId)
    if (like === 1 && !doesUserLike) {
      usersLiked.push(userId)
      sauce.likes += 1
      if (doesUserDislike) {
        const index = usersDisliked.indexOf(userId)
        usersDisliked.splice(index, 1)
      }
    } else if (like === -1 && !doesUserDislike) {
      usersDisliked.push(userId)
      sauce.dislikes += 1
      if (doesUserLike) {
        const index = usersLiked.indexOf(userId)
        usersLiked.splice(index, 1)
      }
    } else if (like === 0 && doesUserLike) {
      const index = usersLiked.indexOf(userId)
      usersLiked.splice(index, 1)
      sauce.likes -= 1
    } else if (like === 0 && doesUserDislike) {
      const index = usersDisliked.indexOf(userId)
      usersDisliked.splice(index, 1)
      sauce.dislikes -= 1
    }
    try {
      await sauce.save()
      res.status(200).json({ message: 'Nombre de likes mis à jour !' })
    } catch (err) {
      res.status(400).json({ err })
    }
  } catch (err) {
    res.status(500).json({ err })
  }
}
