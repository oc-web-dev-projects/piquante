const UserModel = require('./user.model')
const userData = { email: 'test@unit.com', password: 'unittestpassword' }
const uniqueValidator = require('mongoose-unique-validator')

jest.mock('mongoose-unique-validator')

describe('UserModel', () => {
  test('should be invalid is email is empty', async () => {
    const invalidUser = new UserModel({ password: userData.password })
    try {
      await invalidUser.validate(function (err) {
        expect(err.errors.email).toBeDefined()
        expect(err.errors.email.kind).toBe('required')
      })
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should be invalid is password is empty', async () => {
    const invalidUser = new UserModel({ email: userData.email })
    try {
      await invalidUser.validate(function (err) {
        expect(err.errors.password).toBeDefined()
        expect(err.errors.password.kind).toBe('required')
      })
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should be valid if both fields are present', async () => {
    const validUser = new UserModel(userData)
    try {
      await validUser.validate(function (err) {
        expect(err).toBeNull()
      })
    } catch (err) {
      expect(true).toBe(false)
    }
  })
  test('should call unique validator for email field', async () => {
    const validUser = new UserModel(userData)
    try {
      await validUser.validate()
      expect(uniqueValidator).toHaveBeenCalled()
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})
