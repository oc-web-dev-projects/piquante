const SauceModel = require('./sauce.model')
const uniqueValidator = require('mongoose-unique-validator')
const { FactorySauce } = require('../factory_data/sauce_factory')

jest.mock('mongoose-unique-validator')

describe('SauceModel', () => {
  test('should be invalid if required field is empty', async () => {
    const data = new FactorySauce()
    data.name = undefined
    const invalidSauce = new SauceModel(data)
    try {
      await invalidSauce.validate(function (error) {
        expect(error.errors.name.kind).toBe('required')
      })
    } catch (error) {
      console.log(error)
      expect(true).toBe(false)
    }
  })
  test('should be invalid if heat is above 10', async () => {
    const data = new FactorySauce('Tabasco', 11)
    const invalidSauce = new SauceModel(data)
    try {
      await invalidSauce.validate(function (error) {
        expect(error.errors.heat.kind).toBe('max')
      })
    } catch (error) {
      console.log(error)
      expect(true).toBe(false)
    }
  })
  test('should call unique validator for name field', async () => {
    const data = new FactorySauce()
    const sauce = new SauceModel(data)
    try {
      await sauce.validate()
      expect(uniqueValidator).toHaveBeenCalled()
    } catch (error) {
      console.log(error)
      expect(true).toBe(false)
    }
  })
  test('should be valid if required fields are present', async () => {
    const data = new FactorySauce()
    const validUser = new SauceModel(data)
    try {
      await validUser.validate(function (err) {
        expect(err).toBeNull()
      })
    } catch (err) {
      expect(true).toBe(false)
    }
  })
})
