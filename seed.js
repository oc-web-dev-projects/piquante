const faker = require('faker')

const data =
[{
  userId: 1,
  name: faker.name.findName(),
  manufacturer: 'Hoppe, Rosenbaum and Crist',
  description: 'Quasi molestiae ut molestiae sed.',
  mainPepper: 'error',
  imageUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/sethlouey/128.jpg',
  heat: 1
},
{
  userId: 2,
  name: faker.name.findName(),
  manufacturer: 'Gottlieb, Goldner and Bahringer',
  description: 'Mollitia et molestiae nemo corrupti magni debitis.',
  mainPepper: 'quo',
  imageUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/myastro/128.jpg',
  heat: 8
},
{
  userId: 1,
  name: faker.name.findName(),
  manufacturer: 'Predovic - Schumm',
  description: 'Nihil libero quidem adipisci.',
  mainPepper: 'adipisci',
  imageUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/abelcabans/128.jpg',
  heat: 8
},
{
  userId: 3,
  name: faker.name.findName(),
  manufacturer: 'Bergnaum, Luettgen and Langworth',
  description: 'Dolorem aut autem eos numquam commodi assumenda.',
  mainPepper: 'esse',
  imageUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/lebronjennan/128.jpg',
  heat: 6
},
{
  userId: 5,
  name: faker.name.findName(),
  manufacturer: 'Trantow, Lakin and Jerde',
  description: 'Expedita voluptas ut earum.',
  mainPepper: 'et',
  imageUrl: 'https://s3.amazonaws.com/uifaces/faces/twitter/aroon_sharma/128.jpg',
  heat: 7
}]

module.exports = data
