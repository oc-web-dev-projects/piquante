# Piquante API

6th project of the OpenClassrrooms' path: Web Developer

> Build a Secure API for a Review App

Piquante is a new app for customer reviews of specialty hot sauces. Use Node.js, Express, and MongoDB to build its NoSQL database. Secure customer data by applying the OWASP web security standards.

🔧 Skills acquired in this project:

- Enable users to interact with a database using CRUD operations
- Implement a data model
- Store data securely using NoSQL

## FR - Avertissement

Le fichier .env a été commité pour simplifier l'utilisation de l'API à l'évaluateur et aux membres du jury.
Dans d'autres circonstances, ce fichier aurait été listé dans le .gitignore.

### APP

This API works with SoPekocko app :
```bash
git clone https://github.com/OpenClassrooms-Student-Center/dwj-projet6.git
cd dwj-projet6
npm install
npm install node-sass
ng serve
```

## start API

```bash
npm install
npm start
```

### Docker Compose 
  :whale: Run the app and the api with:
  ```bash
  docker-compose up
  ```
Go to http://localhost:4201/


This API was build with TDD using Jest and Supertest

To run the tests : 
```bash
docker-compose exec api npm run test
```

