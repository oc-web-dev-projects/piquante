const userController = require('../controllers/user.controller')
const httpMocks = require('node-mocks-http')
const userRoutes = require('./user.routes')
jest.mock('../controllers/user.controller')

test('should call userController.signup', () => {
  const request = httpMocks.createRequest({
    method: 'POST',
    url: '/signup'
  })
  userRoutes(request)
  expect(userController.signup).toBeCalled()
})

test('should call userController.login', () => {
  const request = httpMocks.createRequest({
    method: 'POST',
    url: '/login'
  })
  userRoutes(request)
  expect(userController.login).toBeCalled()
})
