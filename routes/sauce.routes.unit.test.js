const sauceController = require('../controllers/sauce.controller')
const httpMocks = require('node-mocks-http')
const sauceRoutes = require('./sauce.routes')
const auth = require('../middleware/auth')
const multer = require('../middleware/multer-config')
jest.mock('../controllers/sauce.controller')
jest.mock('../middleware/auth')
jest.mock('../middleware/multer-config')

let req, res, next

beforeEach(() => {
  res = httpMocks.createResponse()
  next = jest.fn()
  auth.mockImplementation((req, res, next) => { return next() })
})
describe('post', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/'
    })
    sauceRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call multer if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/'
    })
    sauceRoutes(req, res, next)
    expect(multer).toBeCalled()
  })
  test('should call multer if auth and multer are ok', () => {
    multer.mockImplementation((req, res, next) => { return next() })
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/'
    })
    sauceRoutes(req, res, next)
    expect(sauceController.createSauce).toBeCalled()
  })
})

describe('get all', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })
    sauceRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call sauceController.getAllSauces if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/'
    })
    sauceRoutes(req, res, next)
    expect(sauceController.getAllSauces).toBeCalled()
  })
})

describe('get one', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id'
    })
    sauceRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call sauceController.getOneSauce if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'GET',
      url: '/:id'
    })
    sauceRoutes(req, res, next)
    expect(sauceController.getOneSauce).toBeCalled()
  })
})

describe('put', () => {
  test('should call auth and multer', () => {
    req = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id'
    })
    sauceRoutes(req, res, next)
    expect(auth).toBeCalled()
    expect(multer).toBeCalled()
  })
  test('should call sauceController.modifySauce if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'PUT',
      url: '/:id'
    })
    sauceRoutes(req, res, next)
    expect(sauceController.modifySauce).toBeCalled()
  })
})
describe('delete', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id'
    })
    sauceRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call sauceController.deleteSauce if auth is ok', () => {
    req = httpMocks.createRequest({
      method: 'DELETE',
      url: '/:id'
    })
    sauceRoutes(req, res, next)
    expect(sauceController.deleteSauce).toBeCalled()
  })
})
describe('post like', () => {
  test('should call auth', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/:id/like'
    })
    sauceRoutes(req, res, next)
    expect(auth).toBeCalled()
  })
  test('should call sauceController.changeLikeSauce', () => {
    req = httpMocks.createRequest({
      method: 'POST',
      url: '/:id/like'
    })
    sauceRoutes(req, res, next)
    expect(sauceController.changeLikeSauce).toBeCalled()
  })
})
