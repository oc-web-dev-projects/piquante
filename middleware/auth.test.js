const auth = require('./auth')
const httpMocks = require('node-mocks-http')
const jwt = require('jsonwebtoken')

let req, res, next

beforeEach(() => {
  req = httpMocks.createRequest()
  res = httpMocks.createResponse()
  next = jest.fn()
})

const tokenVerifier = jest.spyOn(jwt, 'verify')

test('should throw an error if the request userId does not match the decoded token one', () => {
  req.body.id = 35
  req.headers.authorization = 'Bearer 5565843210851545'
  tokenVerifier.mockReturnValue({ userId: 30 })
  try {
    auth(req, res, next)
  } catch (err) {
    expect(err).toBe('Invalid user ID ')
  }
})
test('should call next if  token verifier userId and request id match', () => {
  req.body.id = 35
  req.headers = { authorization: 'Bearer 5565843210851545' }
  tokenVerifier.mockReturnValue({ userId: 35 })
  auth(req, res, next)
  expect(next).toBeCalled()
})
test('shoudl handle errors', () => {
  try {
    auth(req, res, next)
    expect(res.statusCode).toBe(401)
  } catch {
    expect(true).toBe(false)
  }
})
