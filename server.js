const app = require('./app')
const { normalizePort } = require('./helpers/normalize_port')
const { errorHandler } = require('./helpers/error_handler')
const { getConnectionUri } = require('./helpers/database')
const dbHandler = require('./db-handler')

const port = normalizePort(process.env.PORT || '3000')
app.set('port', port)

const server = app.listen(port)

server.on('error', (e) => errorHandler(e, app, port))
server.on('listening', () => {
  const address = server.address()
  const bind = typeof address === 'string' ? 'pipe ' + address : 'port ' + port
  console.log('Listening on ' + bind)
})
dbHandler.connect(getConnectionUri(false))
