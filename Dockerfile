FROM node:14.4

RUN mkdir /api

WORKDIR /api

CMD ["npm", "run", "dev"]
