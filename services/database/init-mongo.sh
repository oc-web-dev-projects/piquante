#!/bin/bash
set -e

mongo <<EOF
use $DATABASE_TEST_NAME
db.createUser({
  user:  '$DATABASE_TEST_USERNAME',
  pwd: '$DATABASE_TEST_PASSWORD',
  roles: [{
    role: 'readWrite',
    db: '$DATABASE_TEST_NAME'
  }]
})
EOF